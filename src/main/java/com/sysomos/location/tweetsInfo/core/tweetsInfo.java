package com.sysomos.location.tweetsInfo.core;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.sysomos.location.tweetsInfo.data.FetchTweets;

public class tweetsInfo {
	private static final String HDFS_LOC = "/twitter/tweets/";
	private static int[] monthDays = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	public static class Parameters {
		@Parameter(names = { "-year", "-y" }, required = true, description = "yyyy")
		private String year;
		@Parameter(names = { "-month", "-m" }, required = true, description = "mm")
		private int month;
		@Parameter(names = { "-day", "-d" }, required = false, description = "dd")
		private int day = -1;
		@Parameter(names = { "-hour", "-h" }, required = false, description = "hh")
		private int hour = -1;
		@Parameter(names = { "-length",
				"-l" }, required = false, description = "length from the the time you specified, unit of length is the min unit you specified")
		private int length = 1;
	}

	public static void main(String args[]) {
		Parameters params = new Parameters();
		new JCommander(params, args).parse();
		String year = params.year;
		int month = params.month;
		int day = params.day;
		int hour = params.hour;
		for (int i = 0; i < params.length; i++) {
			String format = "%s/%02d";
			String hdfs_loc_filepath = HDFS_LOC;
			int temp = Integer.valueOf(year);
			if (temp % 4 == 0 && (temp % 100 != 0 || temp % 400 == 0)) monthDays[2] = 29;
			else monthDays[2] = 28;
			if (params.hour != -1 && params.day != -1) {
				format += "/%02d/%02d";
				hdfs_loc_filepath += String.format(format, year, month, day, hour);
				hour += 1;
				if (hour >= 24) {
					day += 1; hour = 0;
				}
				if (day > monthDays[month]) {
					day = 1; month += 1;
				}
				if (month > 12) {
					year = String.valueOf((Integer.valueOf(year) + 1));
					month = 1;
				}
			} else {
				if (params.day != -1) {
					format += "/%02d";
					hdfs_loc_filepath += String.format(format, year, month, day);
					day += 1;
					if (day > monthDays[month]) {
						day = 1; month += 1;
					}
					if (month > 12) {
						year = String.valueOf((Integer.valueOf(year) + 1));
						month = 1;
					}
				} else {
					hdfs_loc_filepath += String.format(format, year, month);
					month += 1;
					if (month > 12) {
						year = String.valueOf((Integer.valueOf(year) + 1));
						month = 1;
					}
				}
			}
			System.out.println(hdfs_loc_filepath);
			FetchTweets.saveToHive(FetchTweets.fetch(hdfs_loc_filepath));
		}
	//	FetchTweets.index();
	}
	
}