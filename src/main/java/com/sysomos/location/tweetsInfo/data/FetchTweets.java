package com.sysomos.location.tweetsInfo.data;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.hive.HiveContext;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import scala.Tuple2;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.TwitterObjectFactory;

public class FetchTweets {

	static String APP_NAME = "GeoLocation.tweetsInfo.FetchTweets";
	static SparkConf sparkConf;
	static JavaSparkContext sparkContext;
	static HiveContext hiveContext;
	static final int NUM_TASKS = 150;

	static {
		sparkConf = new SparkConf().setAppName(APP_NAME);
		sparkContext = new JavaSparkContext(sparkConf);
		sparkContext.hadoopConfiguration().set("mapreduce.input.fileinputformat.input.dir.recursive", "true");
		hiveContext = new HiveContext(sparkContext.sc());
		System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
	}

	public static JavaRDD<TweetsLoc> fetch(String filepath) {
		System.out.println("Start reading files at " + (new Date()).toString());

		JavaRDD<Status> data = sparkContext.textFile(filepath).map(new Function<String, Status>() {
			@Override
			public Status call(String line) throws Exception {
				Status result = null;
				try {
					result = TwitterObjectFactory.createStatus(line);
				} catch (TwitterException e) {
					System.out.println("Could not parse status");
				}
				return result;
			}

		});
		// System.out.println(data.count());
		System.out.println("Finish reading files at " + (new Date()).toString());
		System.out.println("Start filtering tweets with no place Info at " + (new Date()).toString());
		JavaRDD<TweetsLoc> simplifiedData = data.filter(new Function<Status, Boolean>() {
			@Override
			public Boolean call(Status tweets) throws Exception {
				if (tweets == null)
					return false;
				if (tweets.getGeoLocation() == null || tweets.getPlace() == null || tweets.getLang() == null
						|| tweets.getUser() == null) {
					return false;
				}
				return true;
			}
		}).map(new Function<Status, TweetsLoc>() {
			@Override
			public TweetsLoc call(Status tweet) throws Exception {
				return new TweetsLoc(tweet.getUser().getId(), tweet.getUser().getScreenName(),
						tweet.getGeoLocation().getLatitude(), tweet.getGeoLocation().getLongitude(), tweet.getLang(),
						tweet.getPlace().getCountry(), tweet.getPlace().getCountryCode(),
						tweet.getPlace().getFullName(), tweet.getPlace().getPlaceType(), tweet.getCreatedAt());
			}
		});
		System.out.println("Finish filtering tweets with no place Info at " + (new Date()).toString());
//		getCityList(simplifiedData);
		return simplifiedData;
	}

	
	public static void getCityList(JavaRDD<TweetsLoc> data) {
		JavaRDD<String> cities = data.mapToPair(new PairFunction<TweetsLoc, String, Long>() {

			@Override
			public Tuple2<String, Long> call(TweetsLoc loc) throws Exception {
				return new Tuple2<String, Long>(loc.place_name + ", " + loc.country_code, 1L);
			}
		}).groupByKey().map(new Function<Tuple2<String, Iterable<Long>>, String>() {

			public String call(Tuple2<String, Iterable<Long>> place_name) throws Exception {
				return place_name._1;
			}	
		});
		
	}
	
	public static void saveToHive(JavaRDD<TweetsLoc> data) {
		hiveContext.sql("USE lookup_tables");
		hiveContext.sql("CREATE TABLE IF NOT EXISTS tweets_geo (id BIGINT, screen_name STRING, latitude DOUBLE, longitude DOUBLE, lang STRING, country STRING, country_code STRING, place_name STRING, place_type STRING, date TIMESTAMP)");
		List<StructField> fields = new ArrayList<StructField>();
		fields.add(DataTypes.createStructField("id", DataTypes.LongType, false));
		fields.add(DataTypes.createStructField("screen_name", DataTypes.StringType, false));
		fields.add(DataTypes.createStructField("latitude", DataTypes.DoubleType, false));
		fields.add(DataTypes.createStructField("longitude", DataTypes.DoubleType, false));
		fields.add(DataTypes.createStructField("lang", DataTypes.StringType, false));
		fields.add(DataTypes.createStructField("country", DataTypes.StringType, false));
		fields.add(DataTypes.createStructField("country_code", DataTypes.StringType, false));
		fields.add(DataTypes.createStructField("place_name", DataTypes.StringType, false));
		fields.add(DataTypes.createStructField("place_type", DataTypes.StringType, false));
		fields.add(DataTypes.createStructField("date", DataTypes.TimestampType, false));
		
		StructType schema = DataTypes.createStructType(fields);
		System.out.println("Start inserting data into Hive at " + (new Date()).toString());
		JavaRDD<Row> rowRDD = data.map(new Function<TweetsLoc, Row>() {

			@Override
			public Row call(TweetsLoc data) throws Exception {
				return RowFactory.create(data.id, data.screen_name, data.latitude, data.longitude, data.lang, data.country, data.country_code, data.place_name, data.place_type, new Timestamp(data.date.getTime()));
			}
			
		});
		DataFrame df = hiveContext.createDataFrame(rowRDD, schema);
		df.write().insertInto("tweets_geo");
		System.out.println("Finish inserting data into Hive at " + (new Date()).toString());
	}
	public static void index() {
		hiveContext.sql("USE lookup_tables");
		DataFrame index = hiveContext.sql("SHOW index ON tweets_geo");
		System.out.println("Start indexing data at " + (new Date()).toString());
		if (index.count() != 0) {
			hiveContext.sql("ALTER INDEX IDIndex ON tweets_geo REBUILD");
		}
		else {
			hiveContext.sql("CREATE INDEX IDIndex ON TABLE tweets_geo (id) AS 'COMPACT' WITH DEFERRED REBUILD");
			hiveContext.sql("ALTER INDEX IDIndex ON tweets_geo REBUILD");
		}
		System.out.println("Finish indexing data at " + (new Date()).toString());
	}
}