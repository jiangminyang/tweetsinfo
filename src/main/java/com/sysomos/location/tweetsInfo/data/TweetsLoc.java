package com.sysomos.location.tweetsInfo.data;

import java.io.Serializable;
import java.util.Date;

public class TweetsLoc implements Serializable {
	public Long id;
	public String screen_name;
	public Double latitude;
	public Double longitude;
	public String lang;
	public String country;
	public String country_code;
	public String place_name;
	public String place_type;
	public Date date;
	public TweetsLoc(Long id, String screen_name, Object latitude, Object longitude, String lang, String country, String country_code, String place_name, String place_type, Date date) {
		this.id = id;
		this.screen_name = screen_name;
		this.latitude = Double.valueOf(latitude.toString());
		this.longitude = Double.valueOf(longitude.toString());
		this.lang = lang;
		this.country = country;
		this.country_code = country_code;
		this.place_name = place_name;
		this.place_type = place_type;
		this.date = date;
	}
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("['" + this.id + "','");
		builder.append(this.screen_name + "','");
		builder.append(this.latitude + "','");
		builder.append(this.longitude + "','");
		builder.append(this.lang + "','");
		builder.append(this.country + "','");
		builder.append(this.country_code + "','");
		builder.append(this.place_name + "','");
		builder.append(this.place_type + "']'");
		return builder.toString();
	}
}